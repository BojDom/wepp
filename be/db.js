 const {observable, computed } = require('mobx'),
    fs = require('fs'),
     _ = require('lodash'),
    chalk = require('chalk'); 
 

class dbConnect {

    constructor() {
        this.MongoClient = require('mongodb').MongoClient;
        this.obs = observable.object({m:false,rs0:false,currentDb:'m',megaNz:false});
        this.mongoConnect({name:'m'})
        //this.redisConnect({name:'redis'})
        this.m = computed(()=>this.obs[this.obs.currentDb]) ;
    }

    async mongoConnect(config){
        if (this.obs[config.name] ) {
            console.log(chalk.bgMagenta('sovrascrivendo',config.name))
            this.obs[config.name].close &&  this.obs[config.name].close();
            this.obs[config.name]=false
        }
        let conf = _.defaultsDeep({}, config, {
                url:process.env.MONGO_URL,
                options:{
                    autoReconnect:true,
                    reconnectInterval:4000,
                    reconnectTries: Number.MAX_VALUE,
                    connectTimeoutMS: 30000,
                    readPreference:'nearest'
                }
        })
        
        if (conf.ssl) {
            (()=>{
                if (!fs.existsSync((process.env.MONGOSSLD || '/mongossl/')+'/m/client.pem')) {
                    console.log(chalk.red('no mongo client.pem'))
                    return;
                }
                conf.url += '/?ssl=true';
                Object.assign(conf.options,{       
                    sslKey: fs.readFileSync((process.env.MONGOSSLD ||'/mongossl/')+'/m/client.pem'),
                    sslCert: fs.readFileSync((process.env.MONGOSSLD || '/mongossl/')+'/m/client.pem'),
                    sslCA: fs.readFileSync((process.env.MONGOSSLD || '/mongossl/' ) + '/m/ca.pem'),
                    user:"CN=*.beapoker.pro,OU=client,O=beapoker.pro",
                    auth: { authMechanism: "MONGODB-X509" },
                    ssl:true,
                    //sslValidate: true,
                })
            })();
        }

        this.MongoClient.connect(conf.url , conf.options)
        .then(conn=>{
            console.log(chalk.bgGreen('mongo connesso',config.name,config.url))

            conn.on('error',(e)=>{
                this.obs[config.name]=false;
                console.log(chalk.bgMagenta(conf.url,e))
            })
            conn.on('close',()=>{
                this.obs[config.name]=false;
                console.log(chalk.bgRed(conf.name,'closedd'));
                delete this.MongoClient;
                //throw new Error('closedd')
            })
            if (config.name) this.obs[config.name]= conn
                else throw ('no name defined');
            if (this.obs.currentDb=='m') this.obs.currentDb=config.name
        })
        .catch(e=>{
            console.log(chalk.bgRed(conf.url,e));
            if (this.obs.currentDb!=='m') this.obs.currentDb='m';
            setTimeout(()=>{
                //this.mongoConnect(config)
            },5000)
        });
    }
}



module.exports = new dbConnect()

