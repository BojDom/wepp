
const {unprotect,applySnapshot,onSnapshot,getSnapshot} = require('mobx-state-tree'),
    {Subject}=require('rxjs'),
    {ObjectId} = require('mongodb'),
    {debounce,get} = require('lodash'),
    {when,reaction,observable} = require('mobx'),
    {cartStore,cartDefault} = require('./cartStore'),
    db = require('../db'),
    {bufferTime,filter}= require('rxjs/operators');

module.exports.resetCart = async (socket)=>{
  Object.keys(cartDefault).forEach(k=>{
    socket.cart[k] = cartDefault[k];
  })
}
module.exports.socketCart = async (socket)=>{
    
    socket.cart = cartStore.create( cartDefault);
    socket.reactions = [];
    unprotect(socket.cart);
    
    socket.reactions.push(onSnapshot(socket.cart,debounce(cart=>{
      socket.emit(`SET_CART`,cart);
      
        if (!socket.authToken.guest) {

          if (!cart.spedizione) delete cart.spedizione;
          if (!cart.pagamento) delete cart.pagamento;
          mdb.collection('usr').updateOne({_id:ObjectId(socket.authToken._id)},
              {$set:{cart}},
              {upsert:1})

        }

      },100,{
          maxWait:1000,
          trailing:false,
          leading:true
      })))
   
    
    socket.reactions.push(reaction(()=>socket.cart.totali,(data,r)=>{
      socket.emit(`UPDATE_TOTALI`,data)
    }));

    let order = socket.usr.get().cart;
    if (order ){
      if (!order.spedizione) delete order.spedizione;
      if (!order.pagamento) delete order.pagamento;
      order.items = order.items.filter(i=>{
        return Object.keys(global.data.prodotti).includes(i._id)
      })
      applySnapshot(socket.cart,order);
    }
    return;
}
