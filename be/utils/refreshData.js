const db = require('../db'),
    {zipObject,omit,get} = require('lodash'),
    hash = require('object-hash'),
    {join} = require('path'),
    {ObjectID} = require('mongodb'),
    moment = require('moment'),
    {observable} = require('mobx'),
    {friendlyUrl,fetchFiles, fetchImg,mapImgToName,createLogo} = require('./index');
    

  global.data = {};
  let data = {
    imgs:fetchImg,
    files:fetchFiles,
    categorie:()=>mdb.collection('categorie').find().toArray(),
    spedizioni:()=>mdb.collection('spedizioni').find().toArray(),
    pagamenti:()=>mdb.collection('metodiDiPagamento').find().toArray(),
    prodotti:()=>mdb.collection('prodotti').find().toArray(),
    gruppi:()=>mdb.collection('gruppi').find().toArray(),
    buoni:()=>mdb.collection('buoni').find().toArray(),
    pagine:()=>mdb.collection('pages').find().toArray(),
    homeAlerts:()=>mdb.collection('alerts').find().toArray(),
    siteOptions:()=>mdb.collection('siteOptions').findOne({_id:1}),
  }
  global.data.hashes = observable.map({});
  global.data.uploadImgDirectory =join(__dirname,'../../fe/public/uploadImg/');
  global.data.uploadFilesDirectory =join(__dirname,'../../fe/public/uploadImg/files/');

  global.refreshData = async (arr,scServer)=>{
    if (!arr) {
        global.data.imgs = await data.imgs();
        global.data.files = await data.files();
        arr =  Object.keys(data).slice(2);
    }
    await Promise.all(arr.map(async k=>{
      try{
      switch(k) {
        case 'categorie' : {
            let c =  await data.categorie();

            let cHash = c.map(e=>e.updated).sort();
            global.data.hashes.set('categorie',hash(cHash));
            mapImgToName(c);
          
            c.forEach(cc=>{
              cc.friendlyUrl = friendlyUrl(cc.name);

            });
            global.data.categorie=c;
            break;
        }
        case 'pagamenti' : {
          let c =  await data.pagamenti();
          mapImgToName(c)
          global.data.pagamenti=c;
          break;
        }
        case 'spedizioni' : {
            let c =  await data.spedizioni();
            mapImgToName(c)
            global.data.spedizioni=c;
            break;
        }
        case 'pagine' : { 
            let p  = await data.pagine();

            let pHash = p.map(e=>e.updated).sort();
            console.log('pHASH',pHash,hash(pHash))
            global.data.hashes.set('pagine',hash(pHash));
            mapImgToName(p);
            global.data.pagine=p;
            break;
        }
        case 'prodotti' : { 
            let p = await data.prodotti();
            
            let pHash = p.map(e=>e.updated).sort();
            global.data.hashes.set('prodotti', hash(pHash));
            mapImgToName(p)
            p.forEach(pp=>{

              pp.files = pp.files && pp.files.map && pp.files.map(id=>
                ({...Object.values(global.data.files).find(f=>String(f._id)==String(id))})
              ) || []
              pp.friendlyUrl = friendlyUrl(pp.name)
              if (!get(pp,'thumb.name')) pp.thumb = get(pp,'img.0') || {};
            })
            p = zipObject(p.map(p=>p._id),p);
            global.data.prodotti=p;
            break;
        }
        case 'gruppi': {
          let p = await data.gruppi();
          global.data.gruppi = zipObject(p.map(p=>p.name),p.map(p=>omit(p,['name'])))
          break;
        }
        case 'imgs':{
          global.data.imgs = await data.imgs();
          break;
        }
        case 'files':{
          global.data.files = await data.files();
          break;
        }
        case 'siteOptions':{
          global.data.siteOptions = await data.siteOptions();
          global.data.siteOptions.uploadPath = '/public/uploadImg/';
          global.data.siteOptions.publicPath = '/public/';
          createLogo();
          global.data.siteOptions.fbAppId = process.env.FB_APP_ID;
          mapImgToName(global.data.siteOptions.banners || []);
          scServer && scServer.exchange.publish('refreshData',{[k]:global.data[k]})
          break;
        }
        case 'homeAlerts':{
          global.data.homeAlertsAll = await data.homeAlerts();
          global.data.homeAlerts = global.data.homeAlertsAll.filter(a=>a.enabled&&moment(a.end).isAfter(moment()) && moment(a.start|| '0000-01-01').isBefore(moment()))
          break;
        } 
      }
    
      }catch(e){console.error(k,e)}
      return;
    }))
    if (scServer) {
      scServer.exchange.publish('refreshData',{keys:arr})
    }
  }