const {types} = require('mobx-state-tree'),
  _ = require('lodash');
let custom = name=>types.custom({
  name,
  fromSnapshot(v) {return v},
  toSnapshot(v) {return v},
  isTargetType(v) {return true},
  getValidationMessage(v) {return true}
})
module.exports.cartStoreString = (() => {
  console.log('eVALUATING STORE');
  let attrType=types.model({
    name:types.string,
    value:types.number
  })
  let itemStore = types.model('item', {
    _id: types.string,
    qty:types.number,
    attributes:types.map(attrType),
    iva:types.optional(types.number,0),
    price:types.number,
  }).views(self=>({
    get tot (){
      return  self.price+self.iva 
    }
  }));
  let  spedizioneStore = types.model( {
    _id: types.identifier,
    tempi:types.array(types.number),
    prezzo:types.number,
  });
  let pagamentoStore = types.model( {
    _id: types.identifier,
    name:types.string,
    billing_details:types.model({
      address:types.model({
        "city": types.optional(types.string,'',[ null]),
        "country": types.optional(types.string,'',[ null]),
        "line1": types.optional(types.string,'',[ null]),
        "line2": types.optional(types.string,'',[ null]),
        "postal_code": types.optional(types.string,'',[ null]),
        "state": types.optional(types.string,'',[ null]),
      }),
      phone:types.optional(types.string,'',[ null]),
      email:types.optional(types.string,'',[ null]),
      name:types.string,
    }),
    desc:types.optional(types.string,''),
    html:types.optional(types.string,''),
    tipo:types.optional(types.string,''),
    tempi:types.array(types.number),
    type:types.optional(types.string,''),
    tariffe:types.model({
      fix:types.number,
      pct:types.number
    }),
    prezzo:types.number,
  });
  return types.model('cart', {
    spedizioni:types.array(spedizioneStore),
    spedizione:types.maybe(types.reference(spedizioneStore)),
    pagamenti:types.array(custom('pagamenti')),
    pagamentoId:types.optional(types.string,''),
    indirizzo:types.optional(types.string,''),
    items: types.array(itemStore),
    sconto:types.model({
      name:types.string,
      desc:types.string,
      type:types.string,
      val:types.number
    }),
    paymentIntent:custom('paymentIntent')
  })
}).toString();


module.exports.cartActions = self => {
  return ({
      setCart(data){
          _.merge(self,data);
      },
      set(path,v){
          _.set(socket.cart,path,v);
      }
  })
}

module.exports.cartViews = self => ({
  get pagamento(){
    return self.pagamenti.find(p=>p._id==self.pagamentoId);
  },
  get totali(){
    return {
      articoli:self.items.reduce((a,b)=>a+=b.tot,0),
      spedizione:self.spedizione&&self.spedizione.prezzo||0,
      pagamento:self.pagamento&&self.pagamento.prezzo ||0,
      sconto:(()=>{
        return 0;
      })(),
    }
  },
  get peso(){
    return self.items.reduce((tot,a)=>tot+=a.peso,0);
  },
  get tot(){
    return Object.values(self.totali).reduce((tot,k)=>tot+=k,0).toFixed(2);
  }
})
module.exports.cartStore = eval(module.exports.cartStoreString)().actions(module.exports.cartActions).views(module.exports.cartViews)
module.exports.cartDefault = {
  items:[],
  pagamenti:[],
  spedizioni:[],
  sconto:{
    name:'',
    type:'',
    val:0,
    desc:''
  },
  paymentIntent:{}
}