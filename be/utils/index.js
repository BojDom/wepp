const {google} = require( 'googleapis'),
    fs = require('fs'),
    {promisify} = require('util'),
    db = require('../db'),
    glob = promisify(require('glob')),
    existsp = promisify(fs.exists),
    {join,basename} = require('path'),
    FB = require('fb'),
    unzipper = require('unzipper'),
    j = require('jsonwebtoken'),
    {defaultsDeep,get,max} = require('lodash'),
    {friendlyUrl} = require('../../fe/src/store/utils'),
    {when} = require('mobx'),
    {exec} = require('child_process'),
    nodemailer = require('nodemailer'),
    fbAdmin = require('firebase-admin'),
    FormData=require('form-data');

const firebase = fbAdmin.initializeApp({
  credentials:fbAdmin.credential.applicationDefault(),
  databaseURL: "https://wepp-italia.firebaseio.com",
  storageBucket: "wepp-italia.appspot.com",
})


const fbConf = {
    client_id:process.env.FB_APP_ID,
    client_secret:process.env.FB_CLIENT_SECRET,
    display:'popup',
    scope: 'email',
    redirect_uri: `https://${process.env.DOMAIN || 'localhost:8000'}/fbAuth`,
    Promise:require('bluebird'),
    version:'v8.0'
};
const fb = new FB.Facebook(fbConf);
/*******************/
/** CONFIGURATION **/
/*******************/



const auth =new google.auth.OAuth2(
  process.env.GOOGLE_CLIENT_ID ,
  process.env.GOOGLE_CLIENT_SECRET ,
  'https://'+process.env.DOMAIN+'/googleReadCode'
);


module.exports.googleReadCode = async (code)=> {
  const data = await auth.getToken(code);
  const {tokens} = data ;
  const me = await axios
  .get(`https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${tokens.access_token}`,{
      headers: {
        Authorization: `Bearer ${tokens.id_token}`,
      },
    },
  )
  .then(res => res.data)
  
  
  return {
    id: me.id,
    email: me.email || (me.emails && me.emails.length && me.emails[0].value),
    tokens,
  };
}

let urlGoogle= (state) =>{
  const url = auth.generateAuthUrl({
    access_type: 'offline',
    //prompt: 'consent',
    scope: ['https://www.googleapis.com/auth/userinfo.email',],
    state
  });
  return url;
  }
  

let transporter = nodemailer.createTransport({
  //service: 'gmail',
  host: process.env.STMP_HOST||'smtp.zoho.com',
  secure: 'true',
  port: process.env.STMP_PORT||'465',
  auth: {
    pass:process.env.STMP_PWD,
    //type: 'OAuth2', //Authentication type
    user: process.env.STMP_USR, //For example, xyz@gmail.com
  }
  });

var opts = {
	expiresIn:'1d',
	algorithm: 'HS512',
}


let paypalVerify=async (saleRequest)=>{
  gateway.transaction.sale(saleRequest, function (err, result) {
    return {err,result}
  })
}


let jwt = {
  create: function (content, opt) {
		defaultsDeep(opt, opts)
		return j.sign(content,  process.env.JWT_NGINX_KEY || "local", opts);
	},
	verify: function (content, opt) {
		defaultsDeep(opt, opts)
		return new Promise((resolve, reject) => {
			j.verify(content,
        process.env.JWT_NGINX_KEY || "local", opts, (err, obj) => {
					if (err) resolve(false,err);
          resolve(obj);
				});
		});
	}
};
let savePng =  async (source,out,sizes)=>{
  try{
    if (typeof source == 'string') {
      source = sharp(source);
    }
    if (!sizes ||!sizes.width) 
      sizes = await source.metadata();
    if (sizes.width>2000) {
      sizes.width=2000;
      delete sizes.height;
    }

    await source.resize({
      ...sizes,
      kernel: sharp.kernel.lanczos3,
      fit: 'contain',
      position: 'center',
      background:'transparent'
    }).toFile(out);
    return;
  }catch(e){/*  setTimeout(()=>{},3000);console.log('retryng savePng',out,e);  return;*/}
}

let createThumb =  async (source,out,opt)=>{
  if (!opt) opt = { width:200,height:200}
  if (opt.w) opt= { width: opt.w, height: opt.h}
  try{
    let form= new FormData();
    form.append('opt',JSON.stringify(opt));
    form.append('file' , fs.createReadStream(source),'test.jpg');
    await new Promise(r=>{
      form.submit('https://cibarius.edomi.it/resize' , async (err,resp)=>{
        if(err) {return console.log(err);r();}
        let wst= fs.createWriteStream(out);
        resp.pipe(wst);
        r();
      })
    })
    
  }catch(e){ /* setTimeout(()=>createThumb(source,out,sizes),3000);console.log('retryng thumb',out,e);  return;*/}
}
let unzipEditor = async()=>{
  let outdir =  join(global.data.uploadImgDirectory,'cke');
  if (await existsp(outdir)) {
    console.log('EDITOR GIA PRESENTE e');
    return;
  }
  try{
    let ckeDir = join(global.data.uploadImgDirectory,'cke');
    await promisify(fs.mkdir)(ckeDir);
    let ckeZip = await firebase.storage().bucket().getFiles({prefix:'cke.zip'})
    ckeZip[0][0].createReadStream().pipe(unzipper.Extract({ path: ckeDir }))
  }catch(e){
    console.error(e)
  }
}
let fetchFiles = async ()=>{
  console.log('fetcj files')
  if (!(await existsp( global.data.uploadFilesDirectory)))
    await promisify(fs.mkdir)( global.data.uploadFilesDirectory);
  
  let dbFiles = await mdb.collection('files').find().toArray();
  let localFiles = await new Promise(async res=>{
    try{
      let ff =  await firebase.storage().bucket().getFiles({directory:'files/'})
      ff = ff[0].filter(i=>Number(i.metadata.size))
      console.log('bucket files',ff.length)
      await Promise.all(ff.map( async f=>{
        let outfile = dbFiles.find(i=>i._id==f.name.replace('files/',''));
        if (!outfile) {
          console.log('file non nel db',f.name)
          return;
        }
        outfile = join(global.data.uploadFilesDirectory,outfile.name);

        if (await existsp(outfile)) return;
        return new Promise(r=>{
          try{
            f.createReadStream().pipe(fs.createWriteStream(outfile)).on('complete',r)
            setTimeout(()=>{throw('timeout')},11000);  
          }catch(e){console.err(e);r()}
        })
      }));

      res(await glob( global.data.uploadFilesDirectory+'./*'))
  
    }catch(e){
      console.error(e);
      res([])
    }
    })

    localFiles = localFiles.map(i=>basename(i));
    dbFiles = dbFiles.filter(i=>localFiles.includes(i.name));
    let files = {}
    await Promise.all(localFiles.map(async i=>{
    
      i =  dbFiles.find(dbi=>dbi.name==i);
      if (i) {
        i.ext = i.name.split('.').pop();
        files[i.name] = i;
      }
      return;
    }))
    return files;

}
let fetchImg = async ()=>{

  
    if (!(await existsp( global.data.uploadImgDirectory)))
      await promisify(fs.mkdir)( global.data.uploadImgDirectory);
    
    let thumbsDir =  join(global.data.uploadImgDirectory,'thumbs');
    if (!(await existsp( thumbsDir)))
      await promisify(fs.mkdir)( thumbsDir);
    
    else await promisify(exec)(`find ${thumbsDir} -type f -name '*.png' -size -1k -delete`);
    let dbImg = await mdb.collection('images').find().toArray();
    let localimg = [];
    let downloadImgs = ()=>{
      return new Promise(async res=>{
      try{
        
          let ii = await firebase.storage().bucket().getFiles({directory:'img/'})
          ii = ii[0].filter(i=>Number(i.metadata.size))
          console.log('bucket imgs',ii.length)
          await Promise.all(ii.map( async f=>{
              let outfile = dbImg.find(i=>i._id==f.name.replace('img/',''));
              if (!outfile) {
                console.log('bucketfile non sul db',f.name)
                return;
              }
              outfile = join(global.data.uploadImgDirectory,outfile.name);

              if (await existsp(outfile)) return;
              return await new Promise(async r=>{
                try{
                  await f.download({destination:outfile})
                      //await createThumb(outfile,join(thumbsDir,f.name))
                  r();
                }catch(e){
                  console.error(e);r();
                }
              })
          }));

          localimg = await glob( global.data.uploadImgDirectory + '*.png');
          let iconPackDir =join(global.data.uploadImgDirectory,'iconPack');
          try{
             if (!await existsp(iconPackDir)) {    
              let pack = await firebase.storage().bucket().getFiles({prefix:'iconPack.zip'})
              await promisify(fs.mkdir)(iconPackDir);
              pack[0][0].createReadStream().pipe(unzipper.Extract({ path: iconPackDir }))
            }
          }catch(e){console.error(e);}

          res()
            
      }catch(e){
        console.error(e);
        res()
      }
      })
    }
   // if (!localimg.length)
      await downloadImgs();
  console.log('localimg',localimg.length)
    localimg = localimg.map(i=>basename(i));
    dbImg = dbImg.filter(i=>localimg.includes(i.name));
    let imgs = {}
    
    await Promise.all(localimg.map(async i=>{
      if (!(await existsp(join(thumbsDir,i)))){
        console.log('SHOULD CREATE THUMB',  join(global.data.uploadImgDirectory,i) ,join(thumbsDir,i))
        await createThumb(  join(global.data.uploadImgDirectory,i) ,join(thumbsDir,i))
      }
      i =  dbImg.find(dbi=>dbi.name==i);
      if (i ) imgs[i.name] = i;
      return;
    }))
    return imgs;
}
let createLogo =async ()=>{
  if (get(global,'data.siteOptions.logo.originalSrc') && (!(await existsp(join(global.data.uploadImgDirectory,'customlogo.png'))))) {
    createThumb(join(global.data.uploadImgDirectory,global.data.siteOptions.logo.originalSrc),join(global.data.uploadImgDirectory,'customlogo.png'),global.data.siteOptions.logo.size)
  }
}
let mapImgToName = arr=>{
    arr.forEach(a => {
        a.img = a.img && a.img.map && a.img.map(id=>{
            let i = Object.values(global.data.imgs).find(img=>img._id==id);
            return i 
        }).filter(x=>x) || [];
      });
      return arr
}
let mapImgToId = arr=>{
  if (!(['string','object']).includes(typeof arr)) return;
	if (typeof arr=='string') return global.data.imgs[arr];
	return (arr && arr.map && arr.map(name=>global.data.imgs[name] && global.data.imgs[name]._id &&global.data.imgs[name]._id.toString()) ).filter(i=>i);	
}

module.exports={
    fb,
    urlGoogle,
    fbConf,
    jwt,
    createThumb,
    savePng,
    transporter,
    unzipEditor,
    fetchImg,
    friendlyUrl,
    mapImgToName,
    firebase,
    mapImgToId,
    createLogo,
    fetchFiles
}

