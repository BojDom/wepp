var SCWorker = require('socketcluster/scworker');
var express = require('express');
var healthChecker = require('sc-framework-health-check');

require('dotenv').config({path:'/app/.env'})
if (process.env.ENV.startsWith('dev')) {
  let envsdev =require('dotenv').config({path:'/app/.env.dev'});
  Object.assign(process.env,envsdev.parsed)
  //console.log(envsdev,process.env.STRIPE_PUB_KEY)
}

const methods  = require('./methods'),
    adminMethods  = require('./adminMethods'),
    db = require('./db'),
    {get,throttle} = require('lodash'),
    Stripe = require('stripe'),
    {destroy} = require('mobx-state-tree'),
    {when,observable} = require('mobx');
  
require('./utils/refreshData');

class Worker extends SCWorker {
  run() {
    console.log('   >> Worker PID:', process.pid, process.env.DOMAIN, this.options.environment);
    var environment = this.options.environment;
    var app = express();

    var httpServer = this.httpServer;
    var scServer = this.scServer;
    this.methods = methods;
    this.adminMethods = adminMethods;

    if (environment === 'dev') {
      //app.use(require('morgan')('dev'));
    }

    // Add GET /health-check express route
    healthChecker.attach(this, app);
    httpServer.on('request', app);
    app.get("/fbAuth", (req, res,next) => {
			methods.fbReadCode.call(this,req, res)
    });
    app.get("/googleReadCode", (req, res,next) => {
			methods.googleReadCode.call(this,req, res,next)
    });
    
    app.post("/uploadImg",methods.uploadImg);
    app.post("/uploadFiles",methods.uploadFiles);
    
    require('../vue-render')(app)
  
    scServer.addMiddleware(scServer.MIDDLEWARE_HANDSHAKE_SC, (req, next) => { 
      next()

      let t = req.socket.request.url;
      console.log('handshake',t,process.env.ENV,process.env.NODE_ENV)
/*       jwt.verify(t, scServer.options.authKey, {        expiryIn: scServer.options.authDefaultExpiry,        algorithm: scServer.options.authAlgorithm      }, (data, err) => {
        if (data && data._id) req.socket.setAuthToken({_id: data._id})
      }) */
    }); 
  	scServer.addMiddleware(scServer.MIDDLEWARE_SUBSCRIBE,function(req, next) {
				let splitted = req.channel.split('/');
				if (splitted[0]&&splitted[0]=='priv'){
					if (splitted[1]&&splitted[1]==get(req.socket,'authToken._id')) next();
					else next('not.auth.subscribe')
				}
				else 
				next()
    });
  	scServer.addMiddleware(scServer.MIDDLEWARE_PUBLISH_OUT,function(req, next) {
      let splitted = req.channel.split('/');
      if (splitted[0]&&splitted[0]=='priv'){
        if (splitted[1]&&splitted[1]==get(req.socket,'authToken._id')) next();
        else next('not.auth')
      }
      else 
      next()
  }); 
    /*
    In here we handle our incoming realtime connections and listen for events.
    */
    scServer.on('connection', socket=> {
      //if (!socket.authToken)
       // socket.setAuthToken({  _id: 'guest-'+socket.id })
      socket.usr = observable.box({},{deep:false});

      Object.keys(methods).forEach(m=>{
        socket.on(m,async (data,resp)=>{
          try{
            await methods[m].call(this,{socket,data,resp})
          }catch(e){
            console.error(e);resp(e);
          }
        })
      })
      Object.keys(adminMethods).forEach(m=>{
        socket.on('admin/'+m, async (data,resp)=>{
          if (socket[m]) return;
          socket[m] = true;
          try{
            if (socket.authToken.admin)
              await adminMethods[m].call(this,{socket,data,resp});
              delete socket[m];
          }catch(e){console.error(e);resp(e);delete socket[m];}
        })
      })

      //socket.ga = require('./ga')(socket);
      socket.on('disconnect', () => {
        try{
          Object.keys(methods).forEach(m=>socket.off(m))
          Object.keys(adminMethods).forEach(m=>socket.off('admin/'+m));
          socket.reactions.forEach(r=>typeof r=='function'&&r());
          socket.cart && destroy(socket.cart);
        }
        catch(e){console.log('socketcart',socket.cart)}
      })

    });
  }
}
;(async()=>{
  console.log('awaiting db');
  //db.mongoConnect();
  await when(()=>db.obs.m);
  require('./utils').unzipEditor();
  global.mdb = db.obs.m.db(process.env.DBNAME);
  global.stripe = Stripe(process.env.STRIPE_KEY);
  //await new Promise(r=>setTimeout(r,16000))
    await global.refreshData();
  console.log('data refreshedd')
  new Worker();
})();
