const { zip ,interval,Subject} = require('rxjs'),
	_ = require('lodash'),
	db = require('./db'),
	FB = require('fb'),
	{when} = require('mobx'),
	fetch = require('node-fetch'),
	{getSnapshot} = require('mobx-state-tree'),
	{ObjectID} = require('mongodb'),
    formidable = require('formidable'),
	{friendlyUrl} = require('./utils'),
	fs =require('fs'),
	path = require('path'),
	axios=  require('axios'),
	{socketCart,resetCart} = require('./utils/socketCart'),
	{ URLSearchParams } = require('url'),
	apis = require('./utils');

let order = async function({socket,data}){
	data = {
		usr:String(socket.authToken._id),
		timeStamp:Date.now(),
		cart:data.cart ||Object.assign({},getSnapshot(socket.cart)),
		...data
	};
	let s = {
		bonifico:global.data.siteOptions.statiOrdine[0],
		contrassegno:global.data.siteOptions.statiOrdine[1]
	}

	data.cart.pagamentoId =data.cart.pagamentoId;
	data.cart.spedizione = data.cart.pagamenti.find(p=>p._id==data.cart.spedizione);
	if (s[data.cart.pagamento.tipo]) data.stato=s[data.cart.pagamento.tipo]
	delete data.cart.spedizioni;
	delete data.cart.pagamenti;
	
	data.cart.indirizzo = socket.usr.get().addresses.find(a=>String(a._id)==data.cart.indirizzo).data
	data.cart.totali = socket.cart.totali;
	await mdb.collection('orders').insertOne(data);
	this.scServer.exchange.publish('priv/'+socket.authToken._id+'/redirect','/account/order/'+data._id)
}

let maxOrderId= async ()=>{
	let a =  await mdb.collection('orders').find({_id:{$type:"number"}},{sort:[['_id',-1]],limit:1,projection:{_id:1}}).toArray();
	return a && a.length && (Number(a[0]._id) + 1 + ~~(Math.random()*50)) || (10001 + ~~(Math.random()*5000))
}

let formatEmail = async function(html){

	return `<html>
	<body style="width:100%;max-width:600px">
		<div style="width:100%;max-width:600px">
			<table style="width:100%">
				<tr style="background:linear-gradient(to right, hsla(150, 100%, 27%, 0.3) 0, hsl(0, 0%, 100%) 22%, hsl(0, 0%, 94%) 78%, hsla(357, 72%, 47%, 0.2) 100%);">
					<td>
						<table style="background:url('https://${process.env.DOMAIN+global.data.siteOptions.uploadPath}/customlogo.png');
						min-width:150px;min-height:70px;
						background-size: contain;
						background-repeat: no-repeat;
						background-position: center;						
						width:${global.data.siteOptions.logo.size.w}px;
						height:${global.data.siteOptions.logo.size.h}px;
						"></table>
					</td>
				</tr>
				<tr>
					<td style="background:url('https://${process.env.DOMAIN}/public/img/bg.png');min-height:400px;padding:15px;display:block;">

					${html}

					</td>
				</tr>
				<tr style="background:#fff;text-align:center;width:100%;line-height:0.6em;color:#222;">
					<td>
						${global.data.siteOptions.footerHTML}
					</td>
				</tr>
			</table>
			</div>	
	</body></html>`;
}
let registrationMail = async function({socket,data,resp}){
	let activationLink = `https://${process.env.DOMAIN}/registrazione/verify/${data.code}`;
		
	let html = await formatEmail(`
		Benvenuto in ${global.data.siteOptions.title},<br/><br/>
		Ricevi questa email per completare l'attivazione del tuo account associato alla email <b>${data.to}</b>.<br/>
		Questo link é valido solo per 24 ore, successivamente sará necessario riceve una nuova email di registrazione.
		Clicca sul link sottostante per completare la registrazione su ${process.env.DOMAIN}.
		
		<a href="${activationLink}" style="max-width:220px;text-align:center;background:#ce222a;display:block;color:#fff;font-weight:bold;font-size:22px;margin:50px auto;padding:15px;border-radius:10px;">ATTIVA ACCOUNT</a>
		Se non hai richiesto tu la registrazione sul nostro sito puoi contattarci all' indirizzo di posta elettronica ${global.data.siteOptions.adminMail}.<br/><Br/><br/><br/>
	`)

	apis.transporter.sendMail({
			from:(global.data.siteOptions.adminMail || 'admin@'+process.env.DOMAIN),
			to:data.to,
			subject:'Registrazione in '+process.env.DOMAIN,
			html	
		}, function (err, info) {
			console.log(err , info);
		});
}
let restorePasswordEmail = async function({socket,data,resp}){
	let hideEmail = str=> str.replace(/^(.)(.*)(.@.*)$/,
					(_, a, b, c) => a + b.replace(/./g, '*') + c
		)

	let activationLink = `https://${process.env.DOMAIN}/login?step=restorePassword&code=${data.code}&email=${hideEmail(data.email)}`;
		
	let html = await formatEmail(`	
		Hai smarrito la password?<br/><br/>
		Puoi completare il ripristino della passowrd su ${process.env.DOMAIN} attraverso il link che trovi qui sotto

		<a href="${activationLink}" style="max-width:220px;text-align:center;background:#ce222a;display:block;color:#fff;font-weight:bold;font-size:22px;margin:50px auto;padding:15px;border-radius:10px;">Ripristina password</a>
		Se non sei stato tu a voler ripristinare la password e ricevi questa email ripetutatamente contattaci via email a ${global.data.siteOptions.adminMail}.<br/><Br/><br/><br/>
	`)

	return apis.transporter.sendMail({
			from:(global.data.siteOptions.adminMail || 'admin@'+process.env.DOMAIN),
			to:data.email,
			subject:'Recupera le credenziali in '+process.env.DOMAIN,
			html	
		}, function (err, info) {
			console.log(err , info);
		});
}
let commonLogin = async function({socket,usr,resp}){

	usr.orders=[];
	let u = await mdb.collection('usr').findOne({_id:ObjectID(usr._id)})
	if (!u.name) {
		setTimeout(()=>{
			this.scServer.exchange.publish('priv/'+ usr._id +'/redirect',{path:'/registrazione'});
		},300)
	}
	else {
		u.stripeData = u['stripeData_'+process.env.ENV]
		if (!u.stripeData){
			let stripeData = {email: u.email};
			if (u.vatType == 1 ){
				stripeData.tax_id_data=[ {type:'eu_vat',value:u.vat} ];
			}
			u.stripeData = await stripe.customers.create(stripeData);
			await mdb.collection('usr').updateOne({_id:ObjectID(u._id)},{$set:{ ['stripeData_'+process.env.ENV]: u.stripeData}});
		}
	}

	socket.usr.set(u);
	socketCart(socket);
	
	if (!socket.usr) return u;
	socket.setAuthToken({_id:String(u._id),email:u.email,admin:u.admin});

	resp(null,u)
	socket.emit('LOGIN',u);
	
	if (!u.country) {
		let geo;
		try{
			geo = await axios.get('http://ip-api.com/json/'+socket.request.headers['x-real-ip']+'?fields=status,countryCode');
			if (geo.data.status!=='success') throw('')
		}catch (e){
			geo =  await axios.get('https://ipapi.co/'+socket.request.headers['x-real-ip']+'/country').catch(()=>{});
		}
		socket.usr.country = geo && (geo.data.countryCode || geo.data) || 'none';
	} 
}

module.exports= {
	async test({socket,data,resp}){
		let ii = await firebase.storage().bucket().getFiles(JSON.parse(data))
		resp(null,JSON.stringify(ii))
	},

	async tokenLogin({socket,data,resp}){
		
		let u;
		try{
			if (!data.t) throw new Error('guest')
			u = Object.assign({}, await apis.jwt.verify(data.t));
			if (u.guest) throw ('guest')
		}
		catch(e){
			if (u!='guest')
			u = {_id:'guest-'+new Date().toISOString(),guest:1};
			if (!socket.usr) return u;
			socket.emit('LOGIN',u);
			socket.setAuthToken(u)
			return resp(null,u)
		}
		
		commonLogin.call(this,{socket,usr:u,resp})
		
	},
	async classicLogin({socket,data,resp}){ 
		data.email = data.email.toLowerCase();
		let [usr] = await Promise.all([
				mdb.collection('usr').findOne({email:data.email,pwd: data.pwd}),
				new Promise(r=>setTimeout(r,1000)),
			]);

		if (usr) {
			commonLogin.call(this,{socket,usr,resp});
			resp(null,1)	
			this.scServer.exchange.publish('priv/'+ usr._id +'/redirect',{path:'/account'});
		}
		else {
			console.log('login errata',data);
			resp('Password o email errati')
		}
	},	
	async getCart({socket,data,resp}){

		await when(()=>!!socket.usr.get()._id);

		socket.cart.pagamentoId = undefined;
		socket.cart.spedizione = undefined;
		let items = data.i.map(i=>{
			return {...global.data.prodotti[i.item._id],...i.item,qty:i.qty}
		})
		items.forEach(i=>{
			Object.values(i.attributes || {}).forEach(a=>{
				i.price+=a.value
			})
			i.price = i.price * i.qty;

			i.iva = socket.usr.get().piva ? 0 : i.price * .22;
		})

		socket.cart.items=items;
		resp(null,true)
	},
	async sendStripeToken({socket,data,resp}){
		
		let r  =await stripe.customers.createSource(
			socket.usr.get().stripeData.id,
			{source: data.id}
		);
		let pagamenti = socket.usr.get().pagamenti;
		pagamenti.push(r);
		this.methods.setUsr({socket,data:{pagamenti},resp});
		resp(null,r)
	},
	async createSetupIntent({socket,data,resp}){
		let intent = _.get(socket.usr.get(),'stripeData.setupIntents.'+data);
		if (intent){
			return resp(null,intent);
		}
		intent = await stripe.setupIntents.create({
			customer: socket.usr.get().stripeData.id,
			payment_method_types: [data],
		});
		mdb.collection('usr').updateOne({_id:ObjectID(socket.authToken._id)},{$set:{
			['stripeData_'+process.env.ENV+'.setupIntents.'+data]: intent
		}})
		resp(null,intent)
	},
	async updatePaymentMethod({socket,data,resp}){
		if (data.id=='new'){
			delete data.id;
			
			const paymentMethod = await stripe.paymentMethods.create({
				type: 'card',
				card: data,
				billing_details:{
					
				}
			});
			const pm=  await stripe.paymentMethods.attach(
				paymentMethod.id,
				{customer: socket.usr.get().stripeData.id}
			);
		}
		else {

		}
		resp(null,{pm,paymentMethod});
	},
	getPage({socket,data,resp}){
		resp(null,global.data.pagine.find(p=>p.url==data))
	},
	async getBrainTreeToken({socket,data,resp}){
		let t= await gateway.clientToken.generate({
			//customerId: String(aCustomerId)
		  })
		resp(null,t.clientToken);
	},
	async paypalVerify({socket,data,resp}){
		let orderId = await  maxOrderId()
		var saleRequest = {
			amount: socket.cart.tot,
			paymentMethodNonce: data.nonce,
			orderId:String(orderId),
			options: {
			  submitForSettlement: true,
			  paypal: {
				//customField: "PayPal custom field",
				//description: "Description for PayPal email receipt",
			  },
			}
		  };
		  
		let response = await gateway.transaction.sale(saleRequest);
		if (response.success){
			data._id = orderId;
			data.stato = global.data.siteOptions.statiOrdine[1];
			delete response.success;
			Object.assign(data,response)
			order.call(this,{socket,data})
		}
		console.log('results',data)
		resp(null,1)
	},
	async ccVerify({socket,data,resp}){
		let orderId = await maxOrderId();
		var saleRequest = {
			amount: socket.cart.tot,
			paymentMethodNonce: data.nonce,
			orderId:String(orderId),
			options: {
			  submitForSettlement: true,
			}
		  };
		  
		  
		data= await gateway.transaction.sale(saleRequest);
		if (data.success) {
			data.stato = global.data.siteOptions.statiOrdine[1]
			data._id=orderId;
			order.call(this,{socket,data});
		}
		resp(null,data)
	},
	async getPaymentIntent({socket,data,resp}){
		await this.methods.verifyCaptcha(data.token);
		data._id = await maxOrderId();
		let shipping = socket.usr.get().addresses.find(a=>a._id==socket.cart.indirizzo);
		socket.cart.paymentIntent =  await stripe.paymentIntents.create({
			amount: socket.cart.tot * 100,
			currency:'eur',
			customer: socket.usr.get().stripeData.id,
			shipping: {

			}
		});

		resp(null,socket.paymentIntent)
	},
	async getOrders({socket,data,resp}){
		let query = [];
		if (socket.authToken && !socket.authToken.admin) query.push({usr:String(socket.authToken._id)})
		else {
			data && data.usrId && query.push({usr:data.usrId});
			data && data.todo && query.push({stato:global.data.siteOptions.statiOrdine[1]});
		}
		if (data && data._id) query.push({_id:Number(data._id)});

		let o = await mdb.collection('orders').find(query.length ? {$and:query} : {}).sort({timeStamp:1}).toArray();
		resp(null,o)
	},
	getProduct({socket,data,resp}){
		resp(null,Object.values(global.data.prodotti).find(p=>p.friendlyUrl==data))
	},
	async fbLink({socket,data,resp}){	
		let url = apis.fb.getLoginUrl({...apis.fbConf,state: socket.authToken&&socket.authToken._id || socket.id});
		resp(null,url);
	},
	async setUsr({socket,data,resp}){
		socket.usr.set({...socket.usr.get(),...data});
		mdb.collection('usr').updateOne({_id:ObjectID(socket.authToken._id)},{$set:data});
		socket.emit('LOGIN',data)
	},
	async addToWishlist({socket,data,resp}){
		await when(()=>socket.usr.get());
		let w = new Set(socket.usr.get().wishlist) 
		w.add(data);
		let ok = await mdb.collection('usr').updateOne({_id:ObjectID(socket.authToken._id)},{$set:{wishlist:Array.from(w)}});
		resp(null,ok)
	},
	async fbReadCode(req,res){	
		try{
			let resp = await FB.api('oauth/access_token', Object.assign(apis.fbConf, {
				code: req.query.code
			}))
			if (!resp || resp.error) {
				throw new Error(resp.error);
			}
					
			resp = await apis.fb.api('/me', {fields: 'id,name,birthday,email',access_token:resp.access_token});
			console.log('fb resp',resp)

			let usr = await mdb.collection('usr').findOne({$or:[{'fbData.id':resp.id},{email:resp.email}]});
			if (!usr) {
				usr =  await mdb.collection('usr').insertOne({fbData:resp});
				usr=usr.ops[0]
			}

			this.scServer.exchange.publish('priv/'+ req.query.state+"/popupClose",{t: await apis.jwt.create({_id:usr._id})});
		}
		catch(e){
			console.error('fbReadCode',e)
			//l(!resp ? 'error occurred' : resp.error);
			this.scServer.exchange.publish('priv/'+ req.query.state+'/popupClose',null);
		}
		res.end('Chiudere questa finestra.');
	},

	getData({socket,data,resp}){
		if (data && global.data[data])
			resp(null,global.data[data]) 
	},
	checkHash({socket,data,resp}){

		let keys = Array.from(global.data.hashes._data.keys())
		keys = keys.filter(k=> global.data.hashes.get(k) !== data[k]);
		let DoNotSend = ['hashes','prodotti','gruppi','spedizioni'];

		data = _.pick(global.data, keys.filter(k=>!DoNotSend.includes(k)));
		resp(null,{data,keys});
	
	},
	checkProdotti({socket,data,resp}){
		let obj = {};
		Object.keys(global.data.prodotti).forEach(id=>{
			if (global.data.prodotti[id].updated != data[id])
				obj[id]=_.omit(global.data.prodotti[id],['description'])
		});
		let kc = _.chunk(Object.keys(obj),100);
		zip(kc,interval(100)).subscribe(([k])=>{
			this.scServer.exchange.publish('priv/'+ socket.authToken._id +'/updateProdotti',_.pick(obj,k));
		});
		
		let diff = _.difference(Object.keys(data),Object.keys(global.data.prodotti) );
		if (diff.length)
			this.scServer.exchange.publish('priv/'+ socket.authToken._id +'/deleteProdotti',diff);
	},
	async fbTokenLogin({socket,data,resp}){
		if (!data || typeof data!=='string') return;
		fb.setAccessToken(data);
		resp = await fb.api('/me', {fields: 'id,name,email'});
		let usr = await mdb.collection('usr').findOne({'fbData.id':resp.id});
		if (!usr) return;
		commonLogin.call(this,{socket,usr,resp})
	},
	urlGoogleLogin({socket,data,resp}){
		return resp(null,apis.urlGoogle(socket.authToken._id))
	},
	async googleReadCode(req,res){
		try{
			let resp = await apis.googleReadCode(req.query.code);
			let usr = await mdb.collection('usr').findOne({$or:[{'gData.id':resp.id},{'email':resp.email}]});
			if (!usr) {
				usr = await mdb.collection('usr').insertOne({gData:resp});
				usr = usr.ops[0];
			}
			this.scServer.exchange.publish('priv/'+ req.query.state+'/popupClose',{t: await apis.jwt.create({_id:usr._id})});
		}catch(e){
			console.error('googlereadCode',e)			
			this.scServer.exchange.publish('priv/'+ req.query.state+'/popupClose',{});
		}
		return res.end('ok')
	},
	async deleteIndirizzo({socket,data,resp}){
		let ok = await mdb.collection('usr').findOneAndUpdate({_id:ObjectID(socket.authToken._id)},{$pull:{addresses:{_id:data}}});
		return resp(null,{ok})
	},
	async salvaIndirizzo({socket,data,resp}){
		
		let add = _.omit(data,['loading','_id']);
		
		let {addresses}= await mdb.collection('usr').findOne({_id:ObjectID(socket.authToken._id)},{projection:{addresses:1}});
		if (!addresses) addresses = [];
		if (data._id=='new') {
			if (addresses.find(a=>JSON.stringify(_.omit(a,['_id']))==JSON.stringify(_.omit(a,['_id'])))) {
				return resp('existing');
			}
			addresses.push({
				_id:ObjectID(),
				...add	
			})
		}
		else {
			let toUpdate = addresses.find(a=>a._id==data._id);
			if (toUpdate)Object.assign(toUpdate,add);
		}
		await mdb.collection('usr').updateOne({_id:ObjectID(socket.authToken._id)},{$set:{addresses}});
		
		socket.usr.set(Object.assign(socket.usr.get(), {addresses}));
		resp(null,addresses)
	},
	async verifyEmail({socket,data,resp}){
		if (!data) return resp("no code");;
		let u = await apis.jwt.verify(data);
		if (!u) return resp("bad jwt");
		let e =  await mdb.collection('usr').updateOne({_id:ObjectID(u._id)},{$rename:{emailDaVerificare:'email'}});
		if (!e.result.nModified)  throw('Email gia verificata');
		let usr = await mdb.collection('usr').findOne({_id:ObjectID(u._id)});
		await commonLogin.call(this,{socket,usr,resp});
		resp(null,1)
	},
	async retrievePassword({socket,data,resp}){
		let v = await this.methods.verifyCaptcha(data.token);
		let email = await mdb.collection('usr').findOne({ email: data.email},{projection:{email:1,_id:1}});
		if (!email._id) throw(`Utente non trovato, controlla l'indirizzo email che hai fornito.`)
		data.code = apis.jwt.create({_id:email._id});
		restorePasswordEmail({socket,data,resp})
		resp(null,email)
	},
	async resetPassword({socket,data,resp}){ 
		let v = await this.methods.verifyCaptcha(data.token);
		let u = await apis.jwt.verify(data.code);
		if (data.pwd.length<8) throw ('bad pwd');
		data.pwd = await bcrypt.hash(data.pwd,11)
		u = await mdb.collection('usr').updateOne({_id:ObjectID(u._id)},{$set:{pwd:data.pwd}});
		resp(null,u)
	},
	async resetCart({socket,data,resp}){
		resetCart(socket);
		resp(null)
	},
	async calcolaSpedizioniDisponibili({socket,data,resp}){
		socket.cart.spedizione = undefined;
		socket.cart.spedizioni=[]
		await when(()=>socket.usr.get())	
		let address = socket.usr.get().addresses.find(a=>a._id==data);

		let tipo = address.country=='IT' ? 'nazionale' :'internazionale';
		let spedizioni = global.data.spedizioni.filter(s=>s.tipo==tipo&&s.enabled)
		let peso = socket.cart.peso || 2;
		if (tipo == 'internazionale') {
			//to do calcolo prezzo internazionale
			spedizioni.forEach(sp=>{
				sp.prezzo=0
			})
		}
		else {
			 spedizioni.forEach(sp=>{
				sp._id = String(sp._id);
				if ((sp.gratuitoOltre&&_.get(socket,'usr.cart.tot')>sp.gratuitoOltre) || !_.get(sp,'prezzi.length'))
					return sp.prezzo= 0;
				sp.prezzo = sp.prezzi.filter(s=>!s.peso || s.peso>=peso).sort((a,b)=>b.peso-a.peso)[0].prezzo;
			 })
		}
		await new Promise(r=>setTimeout(r,400))
		socket.cart.spedizioni=spedizioni;
		socket.cart.indirizzo=String(address._id);
		
		//socket.emit('ASSIGN_SERVER_CART',{tot:{spedizione:0}});
		resp(null,spedizioni)
	},
	async getMetodiDiPagamento({socket,data,resp}){
		
		let pagamenti  = global.data.pagamenti.filter(m=>!m.disabled && m.provider!=='stripe');
		let stripePayments = global.data.pagamenti.filter(m=>!m.disabled && m.provider=='stripe');
		
		await new Promise(end=>{
			let done = new Subject();
			zip(stripePayments,done).subscribe(async ([m,n])=>{
				let {data} = await stripe.paymentMethods.list({
					customer: socket.usr.get().stripeData.id,
					type: m.type,
				});
				if (data.length)
				data.forEach(p=>{
				
					pagamenti.push({...m,...p,_id:p.id})
				})
				else pagamenti.push(m)
				n=n+1;
				done.next(n);
				if (n == stripePayments.length) end()
			})
			done.next(0);
		},console.error)
	
		pagamenti = pagamenti.map(p=>({
			...p,
			_id:String(p._id||p.id),
			prezzo:(()=>{
				return socket.cart.totali.articoli * p.tariffe.pct/100 + p.tariffe.fix
			})()
		}));
		socket.cart.pagamenti = pagamenti;
		resp(null,pagamenti)
	},
	async selectedPagamento({socket,data,resp}){
		socket.cart.pagamentoId = data;
	},
	async applyDiscount({socket,data,resp}){
		await new Promise(r=>setTimeout(r,1000))
		resp(null,'')
	},
	async selectedSpedizione({socket,data,resp}){
		let p = socket.cart.spedizioni.find(s=>s._id==data);
		if (!p) return;
		socket.cart.spedizione = data;
	},
	async register({socket,data,resp}){
		await this.methods.verifyCaptcha(data.token);
		delete data.token;
		data.emailDaVerificare = String(data.email).toLowerCase(); delete data.email;
		let emailExists =  await mdb.collection('usr').findOne({$or:[
			//{_id:ObjectID(socket.authToken._id)},
			{email:data.emailDaVerificare},
			{emailDaVerificare:data.emailDaVerificare}
		]},{fields:{_id:1,email:1,emailDaVerificare:1}});
		if (emailExists) {
			if  (emailExists.email) 
				throw (`Email presente , se hai dimenticato la password puoi resettarla nell'area di login`);
			else {
				if (!emailExists.emailDaVerificare) {
					await mdb.collection('usr').updateOne({_id:ObjectID(socket.authToken._id)},{$set:data})
				}
				data.code = apis.jwt.create({_id:emailExists._id});
				data.to = emailExists.emailDaVerificare;
				let mailResponse = await registrationMail.call(this,{socket,data})
				return resp(null,{mailResponse})
			}
		}
		

		data.wishlist = []; delete data.pwd2;
		//data.pwd = bcrypt.hash(data.pwd,process.env.JWT_NGINX_KEY)
		let ok;
		if (socket.authToken.guest)
			ok = await mdb.collection('usr').insertOne(data);
		else 
			ok = await mdb.collection('usr').updateOne({_id:ObjectID(socket.authToken._id)},{$set:data})
		
		data.code = apis.jwt.create({_id:ok.insertedId});
		data.to = data.emailDaVerificare;
		let mailResponse = await registrationMail.call(this,{socket,data})
	
		resp(null,{ok,mailResponse})
		
	},
	async verifyCaptcha(token){
		let body = new URLSearchParams();
		body.append('secret', process.env.RECAPTCHA_SERVER_KEY);
		body.append('response',token);
		let r=  await fetch('https://www.google.com/recaptcha/api/siteverify',{
			method: 'POST',
			body
		}).then(res => res.json());
		if (!r||!r.success) throw (r);
		return r;
	},
	async uploadFiles(req,res,next){

		try {
			let uploadDir =global.data.uploadFilesDirectory;
			new formidable.IncomingForm({
				uploadDir
			}).parse(req, async (err, fields, file) => {
				if (!file.file) file.file = file.upload || file["files[]"];
				if (!file.file){
					console.log('no files', file);
					throw ('no files')
					return;
				}
				let outfile = path.join(uploadDir,file.file.name);
				fs.renameSync(file.file.path, outfile);
				let fileDb = await mdb.collection('files').insertOne({name: file.file.name,type:file.file.type});

				await global.refreshData(['files']);
				res.json({
					data:Object.values(global.data.files).map(i=>({
						src:global.data.siteOptions.uploadPath+'files'+i.name,
					}))
				})
			})
		}
		catch(e){
				console.error(e)
		}
	},
	async uploadImg(req,res,next){

			try {
				let uploadDir = global.data.uploadImgDirectory;
				new formidable.IncomingForm({
					uploadDir,
					//multiples:true,
				}).parse(req, async (err, fields, file) => {
					if (!file.file) file.file = file.upload || file["files[]"];
					if (!file.file){
						console.log('no files', file);
						throw new Exception('no files')
						return;
					}


					file.file.name = friendlyUrl(path.parse(file.file.name).name) + '.png';
					//console.log(err, fields, file);
					let outfile = path.join(uploadDir, file.file.name);
					let thumbfile = path.join(uploadDir, 'thumbs', file.file.name);
					return ;
					let p = await sharp(file.file.path);
					let info = await p.metadata();
					await savePng(p,outfile),
					await createThumb(file.file.path,thumbfile)
				
					fs.unlink(file.file.path, () => {});
					let imgDb = await mdb.collection('images').insertOne({
						name: file.file.name,
						h:info.height,
						w:info.width
					});
					
					await refreshData(['imgs']);
					res.json({
						data:{
							src:global.data.siteOptions.uploadPath + file.file.name,
							type:'image'
						}
					});
				})
					

			} catch (e) {
			  console.log('err uploading',e)
			  res.json({
					data:Object.values(global.data.imgs).map(i=>({
						src:global.data.siteOptions.uploadPath+i.name,
						width:200,
						height:200,
						type:'image'
					}))
				})
			}
		  }
	
}
