const ua = require("universal-analytics");
const _get = require('lodash').get;

module.exports=(socket)=>{
	let session = _get(socket,'authToken._id') || socket.id;
    let ga = ua(process.env.NODE_ENV=='production'?process.env.googleAnalyticsID : '', session, { https: true, strictCidFormat: false });
    ga.u = {}; ga.u.id = socket.id;
    ga.u.ip = 		_get(socket,'request.headers["x-real-ip"]');
    let dr = 		_get(socket,'request.headers.origin');
    let uagent = 	_get(socket,'request.headers["user-agent"]');

    dr && 			ga.set('dr', dr );
    ga.u.ip &&		ga.set('uip', ga.u.ip);
    uagent && 		ga.set('ua', uagent );

    return ga;
}