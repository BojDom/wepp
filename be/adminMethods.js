const _ = require('lodash'),
	db = require('./db'),
	fs = require('fs'),

	{promisify} = require('util'),
	moment = require('moment'),
	unlink = promisify(fs.unlink),
	{exec,spawn} = require('child_process'),
	{join} = require('path'),
	{when} = require('mobx'),
	{ObjectID} = require('mongodb'),
	path = require('path'),
	{jwt,createThumb,mapImgToId,mapImgToName} = require('./utils');

module.exports= {
	async a({socket,data,resp}){
	  
	  
		return resp(null,'aaaa');;
	},
	async getClienti({socket,data,resp}){
		if (!data) data = {admin:{$exists:0}}
		let p = await mdb.collection('usr').find(data).toArray();
		return resp(null,p);
	},
	async saveCliente({socket,data,resp}){
		let c = await mdb.collection('usr').updateOne({_id:ObjectID(data._id)},{$set:{
			gruppo:data.gruppo
		}});
		return resp(null,c.ok)
	},
	async getPage({socket,data,resp}){
		let p = data ? [ global.data.pagine.find(p=>p._id==data) ] : global.data.pagine;
		return resp(null,p)
	},
	async getProdotto({socket,data,resp}){
		let p = data ? [ global.data.prodotti[data] ] : Object.values(global.data.prodotti).map(p=>_.pick(p,['_id','name']));
		return resp(null,p)
	},
	async deletePage({socket,data,resp}){
		let p = await mdb.collection('pages').findAndRemove({_id:ObjectID(data)});
		return resp(null,p.ok)
	},
	async editPage({socket,data,resp}){
		let ok;
		if (!data.url &&data.name!='HOME') return;
		data.updated = Date.now();
		data.img = mapImgToId(data.img);
		if (data._id=='new')
			ok = await mdb.collection('pages').insertOne(_.omit(data,['_id']));
		else ok =await mdb.collection('pages').updateOne({_id:ObjectID(data._id)},{$set:_.omit(data,['_id'])});
		resp(null,ok.ok);
		return global.refreshData(['pagine'],this.scServer)
	},
	async getGruppi({socket,data,resp}){
		data = data ? {_id:ObjectID(data)} :{};
		let p = await mdb.collection('gruppi').find(data).toArray();
		return resp(null,p)
	},
	async getAlerts({socket,data,resp}){
		let p = await mdb.collection('alerts'	).find({}).toArray();
		return resp(null,p)
	},
	async deleteGruppo({socket,data,resp}){
		let p = await mdb.collection('gruppi').findAndRemove({_id:ObjectID(data)});
		return resp(null,p.ok)
	},
	async saveGruppo({socket,data,resp}){
		let ok;
		if (data._id=='new')
		 ok = await mdb.collection('gruppi').insertOne(_.omit(data,['_id']));
		else ok =await mdb.collection('gruppi').updateOne({_id:ObjectID(data._id)},{$set:_.omit(data,['_id'])});
		return resp(null,ok.ok);
		global.refreshData(['gruppi'],this.scServer)
	},
	async saveAlert({socket,data,resp}){
		let ok;
		data.updated=Date.now();
		if (data._id=='new')
		 ok = await mdb.collection('alerts').insertOne(_.omit(data,['_id']));
		else ok =await mdb.collection('alerts').updateOne({_id:ObjectID(data._id)},{$set:_.omit(data,['_id'])});
		resp(null,ok.ok);
		return global.refreshData(['homeAlerts'],this.scServer)
	},
	async getCategorie({socket,data,resp}){
		let c = data ? global.data.categorie.find(c=>c._id==data) : global.data.categorie;
		return resp(null,c)
	},
	async deleteCategoria({socket,data,resp}){
		let p = await mdb.collection('categorie').findAndRemove({_id:ObjectID(data)});
		resp(null,p.ok);
		return global.refreshData(['categorie'],this.scServer)
	},
	async deleteProdotto({socket,data,resp}){
		let p = await mdb.collection('prodotti').findAndRemove({_id:ObjectID(data)});
		resp(null,p.ok);
		return global.refreshData(['prodotti'],this.scServer)
	},
	async getPagamenti({socket,data,resp}){
		return resp(null,global.data.pagamenti)
	},
	async deletePagamento({socket,data,resp}){
		let p = await mdb.collection('metodiDiPagamento').findAndRemove({_id:ObjectID(data)});
		resp(null,p.ok);
		return global.refreshData(['pagamenti'],this.scServer)
	},
	async sortCategorie({socket,data,resp}){

		
		resp(null,ok.ok);
		return global.refreshData(['categorie'],this.scServer);
	},
	async sortBanners({socket,data,resp}){
		let  {banners} = await mdb.collection('siteOptions').findOne({_id:1},{projection:{banners:1}})
		banners = data.map(id=>banners.find(b=>b._id==id));
		let ok = await mdb.collection('siteOptions').updateOne({_id:1},{$set:{banners}});
		await global.refreshData(['siteOptions'],this.scServer);
		resp(null,ok.ok);
		return
	},
	async savePagamento({socket,data,resp}){
		let ok;

		data.img = mapImgToId(data.img)
		if (data._id=='new'){
			delete data._id;
		 	ok = await mdb.collection('metodiDiPagamento').insertOne(data);
		}
		else ok =await mdb.collection('metodiDiPagamento').updateOne({_id:ObjectID(data._id)},{$set:_.omit(data,['_id'])});
		resp(null,ok.ok);
		return global.refreshData(['pagamenti'],this.scServer)
	},

	async cloneProduct({socket,data,resp}){
			let p = await mdb.collection('prodotti').findOne(ObjectID(data));
			p._id = ObjectID(ObjectID(p._id).generationTime)
			let ok = await mdb.collection('prodotti').insertOne(p);
			await global.refreshData(['prodotti'],this.scServer);
			return resp(null,ok);
			
	},
	async saveProdotto({socket,data,resp}){
		if (!data.name ) throw('no name');
		let ok;
		data.img = mapImgToId(data.img);
		data.thumb = mapImgToId(data.thumb);
		data.files  = data.files && data.files.map && data.files.map(name=>global.data.files[name]&&global.data.files[name]._id).filter(x=>x);
		data.updated = Date.now();
		if (data._id=='new'){
			data._id =  String(Number(_.max(Object.keys(global.data.prodotti)) ) + 1);
		 	ok = await mdb.collection('prodotti').insertOne(data);
		}
		else ok =await mdb.collection('prodotti').updateOne({_id:ObjectID(data._id)},{$set:_.omit(data,['_id'])},{upsert:true});
		resp(null,ok);
		global.refreshData(['prodotti'],this.scServer);
		return;
	},
	async saveCategoria({socket,data,resp}){
		if (!data.name ) throw('no name');
		let ok;
		data.img =  mapImgToId(data.img);
		if (Array.isArray(data.prodotti))
		 data.prodotti = data.prodotti.map(id=>String(id)).filter(id=>!!global.data.prodotti[id]);
		data.updated = Date.now();

		
		if (data._id=='new'){
			delete data._id;
			data.parent_id = 0;
			ok = await mdb.collection('categorie').insertOne(data);

			//mdb.collection('categorie').updateOne({_id:0},{$push:{categorie:}})
		}
		else {
			if (data.contiene=='categorie') {
				let oldData = global.data.categorie.find(c=>String(c._id)==data._id);
				let toRemove = _.difference(oldData.categorie,data.categorie);
				let toAdd = _.difference(data.categorie,oldData.categorie)
				Promise.all(toAdd.map(id=>
					Promise.all([
						mdb.collection('categorie').updateOne({_id:ObjectID(id)},{$set:{parent_id:data._id}}),
						//mdb.collection('categorie').updateOne({_id:0},{$pull:{categorie:id}})
					])
				),toRemove.map(id=>
					mdb.collection('categorie').updateOne({_id:ObjectID(id)},{$set:{parent_id:0}})
				))
			}
			ok =await mdb.collection('categorie').updateOne({_id:ObjectID(data._id)},{$set:_.omit(data,['_id'])});
		}

		resp(null,ok);
		await global.refreshData(['categorie'],this.scServer);
		return;
	},
	async getSpedizioni({socket,data,resp}){
		let c = data ? global.data.spedizioni.find(c=>c._id==data) : global.data.spedizioni;
		return resp(null,c)
	},
	async deleteSpedizione({socket,data,resp}){
		let p = await mdb.collection('spedizioni').findAndRemove({_id:ObjectID(data)});
		global.refreshData(['spedizioni'],this.scServer)
		return resp(null,p.ok)
	},
	async getSiteOptions({socket,data,resp}){
		await global.refreshData(['siteOptions'],this.scServer)
		return resp(null,global.data.siteOptions)
	},
	async refresh({socket,data,resp}){
		await global.refreshData(data,this.scServer);
		return resp(null,1);
	},
	async saveSiteOptions({socket,data,resp}){
		data.updated=Date.now();
		if (!data.logo.src.startsWith('customlogo.png')){
			let img = join( global.data.uploadImgDirectory, global.data.imgs[data.logo.src].name);
			if (!data.logo.size || Number(data.logo.size.w)<1) data.logo.size = {w:300,h:70};
			else {
				data.logo.size.w = Number(data.logo.size.w);
				data.logo.size.h = Number(data.logo.size.h);
			}

			let faviconDir = join(global.data.uploadImgDirectory,'../fav/')
			let sizes = [
				512,
				384,
				256,
				192,
				128,
				96,
				72,
				32,
				16,
			]
			await Promise.all(sizes.map(s=>{
				let outfile = faviconDir +'/icon-'+s+'x'+s+'.png';
				return createThumb(img,outfile,{w:s,h:s})
			}))
			await createThumb(img,join(global.data.uploadImgDirectory,'customlogo.png'),data.logo.size);
			data.logo.originalSrc= data.logo.src;
			data.logo.src= 'customlogo.png?updated='+data.updated;
		}
		data.taxes.forEach(t=>{
			if (t._id.startsWith('new')) t._id = ObjectID();
		})
		let ok = await mdb.collection('siteOptions').updateOne({_id:1},{$set:_.omit(data,['_id','banners'])});
		global.refreshData(['siteOptions'],this.scServer)
		return resp(null,ok.ok)
	},
	async saveSpedizione({socket,data,resp}){
		let ok;
		if (!data.name ) throw('no name');
		data.img = data.img && data.img.map && data.img.map(name=>global.data.imgs[name] && global.data.imgs[name]._id &&global.data.imgs[name]._id.toString()) || [];
		data.img = data.img.filter(item => item !== undefined)
		if (data._id=='new'){
			delete data._id;
			//data._id = String(Number(maxBy(global.data.spedizioni,'_id') || global.data.spedizioni.length ) + 1);
		 ok = await mdb.collection('spedizioni').insertOne(data);
		}
		else ok =await mdb.collection('spedizioni').updateOne({_id:ObjectID(data._id)},{$set:_.omit(data,['_id'])});
		resp(null,ok);
		return global.refreshData(['spedizioni'],this.scServer)
	},
	async getImgs({socket,data,resp}){
		return resp(null,global.data.imgs);
	},
	async getFiles({socket,data,resp}){
		return resp(null,global.data.files);
	},
	async editImg({socket,data,resp}){
		if (!data.name ) throw('no name');
		data.name = data.name+'.png';
		let name = Object.values(global.data.imgs).find(v=>v._id==data._id).name;
		if (!name) return;
		if (data.name!=name){
			//await unlink( join(global.data.uploadImgDirectory,data.old) )
			fs.rename(join(global.data.uploadImgDirectory,name),join(global.data.uploadImgDirectory,data.name),()=>{})
			fs.rename(join(global.data.uploadImgDirectory,'thumbs',name),join(global.data.uploadImgDirectory,'thumbs',data.name),()=>{})
		}
		await mdb.collection('images').updateOne({_id:ObjectID(data._id)},{$set:_.omit(data,['_id'])});
		await global.refreshData(['imgs'],this.scServer)
		return resp(null,'ok')
	},
	async deleteImg({socket,data,resp}){
		
		let ok = await mdb.collection('images').removeOne({_id:ObjectID(data)});
		await Promise.all([unlink( join(global.data.uploadImgDirectory,name) ), unlink( join(global.data.uploadImgDirectory,'thumbs',name) )]);
		await global.refreshData(['imgs'],this.scServer)
		
		return resp(null,ok.ok)
	},
	async deleteFile({socket,data,resp}){

		unlink( join(global.data.uploadFilesDirectory,name) )

		let ok = await mdb.collection('files').removeOne({_id:ObjectID(data)});
		await global.refreshData(['files'],this.scServer)
		global.refreshData(['prodotti'],this.scServer)
		return resp(null,ok.ok)
	},
	async getOrders({socket,data,resp}){
		let o = await mdb.collection('orders').find( data ? {_id:data} : {} ).toArray();
		resp(null,o)
	},
	async saveBanners({socket,data,resp}){
		let banners=  data.map(b=>{
		if (typeof b._id=='string' && b._id.startsWith('new')) b._id= ObjectID();
			b.img = mapImgToId(b.img);
			return b;
		});
		let ok = await mdb.collection('siteOptions').updateOne({_id:1},{
			$set:{banners}
		})
		await global.refreshData(['siteOptions'],this.scServer)
		return resp(null,ok)
	},
	async getBackups({socket,data,resp}){
		//let www = db.obs.megaNz.mounts[0].children.find(f => f.name == process.env.DBNAME).children.find(f => f.name == 'backups');
		return resp(null,www.children.map(c=>c.name))
	},
	async restoreBackup({socket,data,resp}){
		return;
		await when(()=>db.obs.megaNz);
		let www = db.obs.megaNz.mounts[0].children.find(f => f.name == process.env.DBNAME).children.find(f => f.name == 'backups');
		let c = await mdb.listCollections().toArray();
		c = c.map(cc=>cc.name);
		let doNotRestore = ['orders','clienti'];
		_.remove(c,coll=>doNotRestore.includes(coll));
		await Promise.all(c.map(cc=>
			mdb.collection(cc).drop()
		))
		let p = spawn('mongorestore',(process.env.BACKUP_STRING +' --nsExclude "wepp.usr" --nsExclude "wepp.orders"').split(' '));
		p.stderr.on('data', (data) => {
			console.log(`stderr: ${data}`);
		  });
		www.children.find(f=>f.name==data).download().pipe(p.stdin);
		p.on('exit',async ()=>{
			await global.refreshData();
			resp(null,1)
		});
		
	},
	async saveBackup({socket,data,resp}){
		let id = String(moment().unix());

		await when(()=>db.obs.megaNz);
		let p = spawn('mongodump',(process.env.BACKUP_STRING.split(' ')));
		p.stdout.pipe(process.stdout)
		p.stderr.on('data', (data) => {
			console.log(`stderr: ${data}`);
		  });
		  
		p.on('exit', function () {
			console.log('EXIT CODE ',arguments);
		  });
		  p.on('end',function(){
			  console.log('ENDDDD', arguments)
		  })
		let www = db.obs.megaNz.mounts[0].children.find(f => f.name == process.env.DBNAME).children.find(f => f.name == 'backups');
		await new Promise(r=>{
			p.stdout.pipe(www.upload(id)).on('complete',(err,data)=>{
			r()
			})
		})
		return resp(null,true)
	/* 	let fileDb = await mdb.collection('files').insertOne({name: file.file.name,type:file.file.type});
		await when(()=>db.obs.megaNz);
		db.obs.megaNz.mounts[0].children.find(f => f.name == process.env.DBNAME).children.find(f => f.name == 'files');
			fs.createReadStream(outfile).pipe(www.upload(String(fileDb.insertedId))).on('complete',(err,data)=>{
					
		}) */
	}
}
