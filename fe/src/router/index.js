import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);

function _import(view){
	return () =>{
		return	System.import('../views/'+view+'.vue');
	};
}

export function createRouter() {
  return new Router({
    mode: 'history',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
      { name:'home',path: '/', component: _import('home') },
      { name:'admin',path: '/admin*', component: _import('admin')},
      { name:'account',path: '/account', component: _import('account')},
      { name:'wishlist',path: '/wishlist', component: _import('wishlist')},
      { name:'order',path:'/account/order/:id',component : _import('order')},
      { name:'cart',path: '/cart*', component: _import('cart') },
      { name:'search',path: '/search/*', component: _import('search') },
      { name:'registrazione',path: '/registrazione*', component: _import('registrazione') },
      { name:'login',path: '/login', component: _import('login') },
      { name:'catalogo',path: '/catalogo*', component: _import('catalogo') },
      { name:'prodotto',path: '/prodotto*', component: _import('prodotto') },
		  { name:'404',path: '*', component: _import('page') }
    ]
  });
}
