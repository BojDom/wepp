import Vue from 'vue';
import 'es6-promise/auto';
import { createApp } from './app';
import {when} from 'mobx';
import vso from './store/utils/vso/Main';
import scc from 'socketcluster-client';
import Vlf from './store/utils/vlf'
import {get} from 'lodash';
import AsyncComputed from 'vue-async-computed'
import { VueReCaptcha } from 'vue-recaptcha-v3'
import jscookie from 'js-cookie'


Vue.use(AsyncComputed)
Vue.use(Vlf)
 
// For more options see below
Vue.use(VueReCaptcha, { siteKey: RECAPTCHA_KEY })
 
let loadImage = async src=>{
  return new Promise(res=>{
    let img=new Image();
    img.src=src;
    img.onload = ()=>{
      res(src);
    }
    img.onerror= (e)=>{
      console.error(e,src)
      res(false)
    }
  })
    
}
try{
const { app, router, store } = createApp({
  lang: (navigator.language || navigator.userLanguage || 'en' ).substr(0,2).toLowerCase()
});
  
if (ENV == 'development') {
  console.log('WS_HOST', API_HOST)
  window.app=app;
}
Vue.directive('img', {
  bind:async function(el, binding, vnode){

    if (el.src &&await loadImage(el.src)) {
      //console.log('loaded',el.src);
      return;
    }
    el.src= 'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif';
    let url = get(vnode,'data.attrs.src-set');
    let def =  vnode.context.$store.state.siteOptions.uploadPath+'thumbs/customlogo.png';
    let found =false;
    if (Array.isArray(url)) {
      await new Promise(res=>{
        url.forEach(async u=>{
          if (found) return;
          found = await loadImage(u);
          if (found) res();
        })
      });
    }
    el.src = found || def;
  },
  // When the bound element is inserted into the DOM...
 /*  inserted: function (el) {
    // Focus the element
   // console.log('ell',el)
  } */
})

Vue.use( new vso(scc.connect({
  hostname: API_HOST,
  port:443,
  secure: true,
  ackTimeout: 10000 ,
  query:{},
  autoReconnectOptions: {
    initialDelay: 1000, //milliseconds
    randomness: 2000, //milliseconds
    multiplier: 1.5, //decimal
    maxDelay: 4000 //milliseconds
  },
}), store));
// prime the store with server-initialized state.
// the state is determined during SSR and inlined in the page markup.
if (window.__INITIAL_STATE__) {
  store.replaceState(window.__INITIAL_STATE__);
}

// wait until router has resolved all async before hooks
// and async components...
router.onReady(async() => {

  

  // Add router hook for handling asyncData.
  // Doing it after initial route is resolved so that we don't double-fetch
  // the data that we already have. Using router.beforeResolve() so that all
  // async components are resolved.
  /*router.beforeResolve((to, from, next) => {
    const matched = router.getMatchedComponents(to);
    const prevMatched = router.getMatchedComponents(from);
    let diffed = false;
    const activated = matched.filter((c, i) => {
      return diffed || (diffed = (prevMatched[i] !== c));
  });
    if (!activated.length) {
      return next();
    }
    bar.start();
    Promise.all(activated.map(c => {
      if (c.asyncData) {
        return c.asyncData({ store, route: to });
      }
    })).then(() => {
      bar.finish();
      next();
    }).catch(next);
  });*/
    
    app.login = ()=>new Promise(r=>{
      let t = jscookie.get('t')
        app.$socket.emit('tokenLogin',{t},(err,data)=>{
          Object.assign(store.state.usr, data ||{});
          r();
        })
        setTimeout(r, ENV.startsWith('p')?4000:14000);
    });
    await app.login()
    
    console.log('aut',app.$authToken.get()._id,app.$socket.channels,store.state.connState)
    // actually mount to DOM
    
    app.$mount('#app');
    window.app = app;  
});

// service worker
if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
  navigator.serviceWorker.register('/service-worker.js');
}

}
catch(e){
  alert(e)
}