export default {
	"homeTitle": {
		it: "",
		en: ""
	},
	"homeDescription": {
		it: "",
		en: ""
	},
	"hi": {
		it: "ciao",
		en: "hello"
	},
	me:{
		it:"me",
		en:"me"
	},
	"source_not_found": {
		it: "Cé stato un problema pd :( scrivimi che magari lo risolvo al volo",
		en: "Source not found :("
	},
	"converting": {
		it: "Sto convertendo in mp3 aspe",
		en: "Downloading from external resource"
	},
	"source_found": {
		it: "Apposhht solo un attimo",
		en: "Awaiting conversion"
	},
	"search_source": {
		it: "Recupero il link",
		en: ""
	},
	"complete": {
		it: "download pronto :)",
		en: "download ready :)"
	},
	"cut": {
		it: "ritaglia",
		en: ""
	},
	"login_with_facebook": {
		it: "accedi con facebook",
		en: "login with facebook"
	},
	"login": {
		it: "accedi",
	},
	"today": {
		it: "oggi",
	},
	"yesterday": {
		it: "ieri",
	},
	"thisWeek": {
		it: "questa settimana",
		en: "this week"
	},
	"thisMonth": {
		it: "questo mese",
		en: "this month"
	},
	"history": {
		it: "cronologia",
	},
	"following": {
		it: "seguiti",
	},
	"friends": {
		it: "amici",
	},
	"reconnect_msg":{
		it:"Se visualizzi questo messaggio per piú di qualche secondo prova a riaggiornare la pagina",
		en:"if you see this message for more than few seconds try refreshing the page"
	},
	"activate_audio":{
		it:'attiva audio',
		en: 'activate audio'
	},
	"deactivate_audio":{
		it:'disattiva audio',
		en: 'deactivate audio'
	}
}