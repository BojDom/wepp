export default {
  // aggiunge thumbnail dopo la ricerca

  // modifica i thumbnail con la risposta del socket
/*  socket_cProgress:({commit,dispatch,state},v)=>{
    let index=state.thumbnails.find(t=>{
      return (t._id==v.id)
    })
    if (typeof index ==='undefined') return;
    if (v.progress) index.progress=v.progress;
    if (v.msg) index.msg = v.msg;
    if (v.d) index.d = v.d;
    if (v.size) index.size = v.size;
    if (v.wave) index.wave=v.wave;
    if (v.viewCount) index.viewCount;
  },
*/
 isTouch:()=>{
  var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
  var mq = function(query) {
    return window.matchMedia(query).matches;
  }

  if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
    return true;
  }

  // include the 'heartz' as a way to have a non matching MQ to help terminate the join
  // https://git.io/vznFH
  var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
  return mq(query);
}
}