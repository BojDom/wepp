
export default {
  // ids of the items that should be currently displayed based on
  // current list type and current pagination
  isConnected:state=>{
    return (['connect','connected','reconnect']).includes(state.connState)
  },
  cartQty:state=>{
    return state.serverCart &&state.serverCart.items && state.serverCart.items.length || state.cart.items.length || 0
  },
  preferredAddress:state=>{
    let a = state.usr && state.usr.addresses && state.usr.addresses.slice()||[]
    return a.find(aa=>aa.preferred) || a[0]
  },
  metodoDiSpedizione:state=>{
    return state.serverCart.spedizioni.find(p=>p._id==state.serverCart.spedizione)
  },
  metodoDiPagamento:(state)=>{
    let p =  state.serverCart.pagamenti.find(p=>p._id==state.serverCart.pagamento);
    if (!p && state.usr) {

    }
  },
  tot:(state,getters)=>{
    return Object.values(state.serverCart.totali).reduce((arr,v)=>arr+=v,0);
  },
  addresses:state=>{
    return state.usr && state.usr.addresses && state.usr.addresses.slice()||[]
  },
  indirizzoSelezionato:(state,getters)=>{
    return getters.addresses.find(a=>a._id==state.serverCart.indirizzo)
  },
  showPrice(state){
    if (state.usr && state.usr._id) return true;
    return  state.siteOptions.showPriceToGuests
  },
  parsedItems(state){
    return state.cart.items.map(i=>({item:JSON.parse(i.item),qty:i.qty})) || []
  },
  screenSize:state=>{
    switch(true) {
      //case (state.screen.w<360): return 's';
      case (state.screen.w<520): return 'm';
      case (state.screen.w<950): return 't';
      default: return 'd';
    }
  },
}
