import dexie from 'dexie';
export const db = new dexie('fuz');
db.version(1).stores({
    pagine:'&_id,url,name,title,html,updated',
    prodotti:'&_id,name,updated,friendlyUrl,img',
    homeAlerts:'&_id,updated,start,end,text',
    categorie:'&_id,img,updated',
});
export default  {
    install :(Vue, options) =>{
        Vue.prototype.$db = db;

    },
    beforeMount(){
        this.$options.computed.path = ()=>{
            this.$route.path.split('/').slice(1);
        }    
    }
};

