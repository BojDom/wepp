import {findIndex,assign,omit,get,set} from 'lodash';
import {getUrlPath} from './utils';

let ADD_TO_CART = (state,data)=>{
  if (!data.item.attributes) data.item.attributes={};
  let str=JSON.stringify({
    _id:data.item._id,
    attributes:data.item.attributes
  });
  let i = state.cart.items.find(i=>i.item==str);

  if (i) {
    if (data.qty<1){
      let index = findIndex(state.cart.items,i=>i.item===str);
      state.cart.items.splice(index,1);
    }
    else i.qty = data.qty;
  }
  else if (data.qty)
    state.cart.items.push({item:str,qty:data.qty})
}
export default {
  SET_CONNECTION_STATE:(state,conn)=>{
    state.connState=conn
  },
  SOCKET_SET_CART:(state,data)=>{
    if (data.items)
      data.items = data.items.filter(i=>i.qty>0)
    let kData =  Object.keys(data);
    kData.forEach(k=>{
      state.serverCart[k]=data[k]
    });
    (['spedizione','pagamento']).forEach(k=>{
      if (!kData.includes(k)) state.serverCart[k]=null
    })


    data.items && data.items.forEach && data.items.forEach(i=>{
      let item = {attributes:i.attributes,_id:i._id};
      ADD_TO_CART(state,{qty:i.qty,item});
    })
    state.serverCart.test=Math.random()
  },
  SOCKET_ASSIGN_SERVER_CART:(state,data)=>{
    console.log('assignservercart',arguments)
    assign(state.serverCart,data);
    state.serverCart.test=Math.random()

  },
/*   SOCKET_CART_PATCH:(state,data)=>{
    if (!data || !data.length) return;
    data.forEach(p=>{

        if (p.op!='remove') {
          set(state.serverCart ,p.path.split('/').splice(1).join('.'),p.value);
        }
        else{
          let parent = get(state.serverCart,p.path.split('/').slice(1,-1).join('.'));
          remove(parent,(a,k)=>k==p.path.split('/').slice(-1))
        }
    })
    state.serverCart.test=Math.random()
  }, */
 
  SOCKET_UPDATE_TOTALI:(state,data)=>{
    state.serverCart.totali = data;
  },
  ADD_TO_CART,
  STORAGE_CART:(state,data)=>{
    state.cart = data;
  },
  SOCKET_LOGIN:(state,user)=>{
    console.log('socket login',user)
    state.usr=user;
    
    
    //delete user.fbUrl;
    //jscookie.set('t', user.token, { expires: 365 });
  },
  ASSIGN_USR:(state,data)=>{
    Object.assign(state.usr,data);  
  },
  LOGOUT:(state)=>{
    state.usr={}
  },
  /* 
  LOCALSTORAGE_LOGIN:(state)=>{
    try { if (!localStorage) return; state.usr =  {...JSON.parse(localStorage.u),fbUrl:null}; } catch (err) { console.log(err); }
  }, */
  setScreen:(state,size)=>{
    if (size.w) state.screen.w = size.w;
    if (size.h) state.screen.h = size.h;
  },
  setPath:(state,path)=>{
    state.path= getUrlPath(path);
  },
  logout:(state,path)=>{
    Object.keys(state.usr).forEach(k=>state.usr[k]='');
  }

};