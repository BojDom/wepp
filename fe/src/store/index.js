import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'
Vue.use(Vuex);

var window={
   innerHeight:0,
   innerWidth:0
};
export function createStore () {
  return new Vuex.Store({
    state: {
      connState:'',
      locale:'it',
      screen:{w:window.innerHeight,h:window.innerWidth},
      serverCart:{
          totali:{
            articoli:0,
            spedizione:0,
            pagamento:0,
            sconto:0
          },
          test:1,
          items:[],
          pagamenti:[],
          pagamento:"",
          spedizioni:[],
          spedizione:"",
          indirizzo:false,

      },
      usr:{
        token:false
      },
      path:[""],
      cart:{
        buono:'',
        items:[]
      },
      siteOptions:{}
    },
    actions,
    mutations,
    getters
  })
}