let{get} = require('lodash');
module.exports = {
    
   friendlyUrl:(url)=>{
      if (!url || !url.toString()) {
         console.log('friendly url missing');
         return ""
      }
      const a = 'àáäâãåăæçèéëêǵḧìíïîḿńǹñòóöôœṕŕßśșțùúüûǘẃẍÿź·/_,:;'
      const b = 'aaaaaaaaceeeeghiiiimnnnoooooprssstuuuuuwxyz------'
      const p = new RegExp(a.split('').join('|'), 'g')
      return url.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
        .replace(/&/g, '-and-') // Replace & with ‘and’
        .replace(/[^\w\-]+/g, '') // Remove all non-word characters
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, '') // Trim - from end of text
   },
   aClick:function($event) {
      const { target } = $event
      // handle only links that occur inside the component and do not reference external resources
      if (target && target.matches("a:not([href*='://'])") && target.href) {
        // some sanity checks taken from vue-router:
        // https://github.com/vuejs/vue-router/blob/dev/src/components/link.js#L106
        const { altKey, ctrlKey, metaKey, shiftKey, button, defaultPrevented } = $event
        // don't handle with control keys
        if (metaKey || altKey || ctrlKey || shiftKey) return
        // don't handle when preventDefault called
        if (defaultPrevented) return
        // don't handle right clicks
        if (button !== undefined && button !== 0) return
        // don't handle if `target="_blank"`
        if (target && target.getAttribute) {
          const linkTarget = target.getAttribute('target')
          if (/\b_blank\b/i.test(linkTarget)) return
        }
        // don't handle same page links/anchors
        const url = new URL(target.href)
        const to = url.pathname
        if (window.location.pathname !== to && $event.preventDefault) {
          $event.preventDefault()
          this.$router.push(to)
        }
      }
    },
    thumb:function(){
      let {uploadPath} = this.$store.state.siteOptions;
        let d = uploadPath + 'customlogo.png';
        if (get(this.p,'thumb.name')) return [uploadPath+'thumbs/'+this.p.thumb.name,uploadPath+this.p.thumb.name,d];
        if (get(this.p,'img.0.name'))
         return [uploadPath+'thumbs/'+this.p.img[0].name,d];
        else return [ d]
    },
    getUrlPath(url){
        return url.substr(0, url.indexOf('?')>0 ? url.indexOf('?')  : url.length).split('/').slice(1);
    }
}