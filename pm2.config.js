const path=require('path');
var isProd = process.env.ENV == 'production';

console.log('pm2 isProd',isProd)
module.exports = {
    apps: [
        {
            name: "app",
            script: path.join(__dirname, "server.js"),
            //autorestart: true,
            //log_type: "json",
            watch: isProd  || !process.env.DOMAIN ? false : ['be/**.js','./*.js'],
            //interpreter_args:['--inspect=0.0.0.0:9194'],
            args:  isProd ? [] : ['--inspect-workers=0.0.0.0:6178'],
            env_dev:{
                'ENV':'development',
                'NODE_ENV':'development',
            },
            env_prod:{
                'NODE_ENV':'production',
                'ENV':'production'
            }
        },
        {
            name: "http",
            script: path.join(__dirname, "node_modules/myutils/http.js"),
        } 
    ],

    deploy: {}
};