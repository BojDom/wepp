FROM node:alpine
ADD . /app
WORKDIR /app
RUN mkdir -p /app/fe/public/uploadImg/thumbs
RUN chmod 777 /app/fe/public/uploadImg -R
#RUN apt-get update && apt-get install -y python build-essential
RUN apk add --update libc6-compat git make nano g++ mongodb-tools

ENV PATH "$PATH:/app/node_modules/pm2/bin"
CMD ["npm", "start"] 
