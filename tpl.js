const env = require('dotenv').config({path: __dirname + '/.env'}).parsed;

const path = require('path'),
	pkg = require('./package.json');

let host = {
	dev :  process.env.DOMAIN || '192.168.43.187:8000',
	prod:  process.env.DOMAIN || '192.168.43.187:8000',
}

let cfg =  {
	project: pkg.name || "",
	title: pkg.title || "",
	publicPath: "/dist/",
	logo: pkg.logo || path.join(__dirname,'fe/public/img/logo.png'),
	root: path.join(__dirname,'fe'),
	src: path.join(__dirname,'fe/src'),
	indexSsr:  path.join(__dirname,'fe/src/entry-server.js'),
	dev: {
		dist: path.join(__dirname, 'fe/dev'),
		clientVars: {
			API_HOST: JSON.stringify(host.dev),
			RECAPTCHA_KEY: JSON.stringify(env.RECAPTCHA_CLIENT_KEY),
			LOGO: "'/public/fav/logo.png'",
			ENV: "'dev'",
			__THEME__: JSON.stringify('dev'),
			__DEV__:true,
		}
	},
	prod: {

		dist: path.join(__dirname, 'fe/dist'),
		clientVars: {
			API_HOST: JSON.stringify(host.prod),
			RECAPTCHA_KEY: JSON.stringify(env.RECAPTCHA_CLIENT_KEY),
			LOGO: "'/public/fav/logo.png'",
			ENV: "'prod'",
			__THEME__: JSON.stringify('default'),
			__DEV__:false,
		}
	},
	pwaConfig:{
		cacheId:'fapp'
	}
};

module.exports = cfg;